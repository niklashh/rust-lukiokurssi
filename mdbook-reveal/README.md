# `mdbook-reveal`

mdbook renderer that generates revealjs slideshows from each chapter.

## Usage

Compile and install

``` shell
cargo install --path .
```

Run mdbook as usual

``` shell
mdbook serve
```

## Functionality

`mdbook-reveal` assumes html renderer is run first.

> Have not checked whether this is always the case but seems to work for now

### `openPresentation.js`

This script should be included in `book.toml` in order to add the `Open presentation` button to the top bar

### Program

1. markdown content from books is rendered into the `reveal.hbs` `{{ content }}` placeholder.
2. the rendered handlebars templates are copied to `book/html` with extension `.reveal.html`
3. all static files from `static` folder are copied to `book/html/mdbook-reveal/reveal` which are statically served

## TODO

- Get the book title
- Support alternative base path