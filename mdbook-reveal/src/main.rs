use fs_extra::dir::CopyOptions;
use handlebars::Handlebars;
use mdbook::book::BookItem;
use mdbook::renderer::RenderContext;
use mdbook::utils::fs::path_to_root;
use serde_json::json;
use std::collections::BTreeMap;
use std::fs::{create_dir, read_to_string, write};
use std::io;
use std::path::PathBuf;

fn main() {
    eprintln!("Starting mdbook-reveal");
    let mut stdin = io::stdin();
    let ctx = RenderContext::from_json(&mut stdin).unwrap();
    let additional_css: Vec<String> = ctx
        .config
        .get_deserialized_opt("output.html.additional-css")
        .unwrap()
        .unwrap_or_default();
    let book_title = ctx.config.book.title.unwrap_or_default();

    let mut handlebars = Handlebars::new();

    let reveal_html =
        read_to_string(ctx.root.join("mdbook-reveal/src/reveal.hbs")).expect("to find reveal.hbs");
    handlebars
        .register_template_string("reveal", reveal_html)
        .unwrap();

    for item in ctx.book.iter() {
        if let BookItem::Chapter(ch) = item {
            if let Some(ref path) = ch.source_path {
                let dest = PathBuf::from("../html").join(path);
                write(dest.clone(), &ch.content).unwrap();

                let dest_reveal = dest.with_extension("reveal.html");
                let mut data = BTreeMap::new();
                data.insert("content".to_string(), json!(ch.content.clone()));
                data.insert(
                    "title".to_string(),
                    json!(format!("{} - {}", &ch.name, &book_title)),
                );
                data.insert("path_to_root".to_string(), json!(path_to_root(path)));
                data.insert("additional_css".to_string(), json!(additional_css));

                let rendered = handlebars.render("reveal", &data).unwrap();
                eprintln!(
                    "Writing revealed chapter to {:?} ({} bytes)",
                    dest_reveal,
                    rendered.len()
                );

                write(dest_reveal, &rendered).unwrap();
            }
        }
    }

    let reveal_static = ctx.root.join("book/html/mdbook-reveal/reveal");
    create_dir(&reveal_static)
        .expect("unable to create folder for static files in book/html/mdbook-reveal/");

    let mut options = CopyOptions::new();
    options.content_only = true; // don't create extra static directory
    fs_extra::dir::copy(
        ctx.root.join("mdbook-reveal/static"),
        &reveal_static,
        &options,
    )
    .unwrap();
}
