# Rust-ohjelmointikurssi: 5. jakso 8-palkki

Tervetuloa Rust-ohjelmoinnin kurssille!

[Rust](https://rust-lang.org) on [Mozilla Foundationin](https://foundation.mozilla.org) kehittämä staattisesti sekä dynaamisesti tyypitetty olio-orientoitu imperatiivinen funktionaalinen ekspressiivinen korkean tason turvallinen järjestelmäohjelmointikieli, joka on avointa lähdekoodia.

Kurssi on tarkoitettu ohjelmointiin jo tutustuneille oppilaille.

Esimerkkejä ohjelmointikielistä, joiden osaamisesta on hyötyä kurssilla:
- Python
- JavaScript
- Java
- Scala
- C/C++

Jos olet käynyt Aallon Python-kurssin tai Koodi101-kurssin, kurssi on sinulle sopivan tasoinen.

Tällä kurssilla opit matalan tason ohjelmoinnin pääkonseptit, kuten pino, keko ja pointterit. Kurssilla opitaan myös korkean tason ohjelmoinnin konsepteja, kuten abstraktiot, tyypit, omistajuus ja korkeamman asteen funktiot.

Kurssin tavoitteena on oppia lukemaan Rustin dokumentaatiota ja Rustilla kirjoitettua lähdekoodia. Kurssilla tehdään myös projekti, jossa opitaan oman ohjelman ja kirjaston kehittämistä Rustilla.

Kurssilla on paljon tehtäviä, joita pitää ratkaista omalla ajalla. Kurssin voi suorittaa myös osittain itsenäisesti.

Kurssilta ei saa arvosanaa; Läpipääsyvaatimuksena on puolet tehtävistä ja projekti tehtynä kurssin loppuun mennessä.

Kurssilla on käytössä ennennäkemätön kurssia varten kehitetty web-VSCode -kehitysympäristöjärjestelmä ja otarustlings-tehtäväalustajärjestelmä.
