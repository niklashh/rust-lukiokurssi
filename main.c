#include <stdio.h>

int f(int result) {
  return f(-result);
}

int main() {
  printf("%d\n", f(0));
  return 0;
}
