* Viikottainen palautekyselypohja

** Aihekohtainen oppimismatriisi

Arvioi viikolla oppimasi aiheiden osaamistasosi:

| Aihe \ Taso              | 0 (täysin vieras) | 1 | 2 | 3 | 4 | 5 (täysin hallussa) |
|--------------------------+-------------------+---+---+---+---+---------------------|
| Omistajuus               |                   |   |   |   |   |                     |
| Funktiot                 |                   |   |   |   |   |                     |
| Enumeraatiot             |                   |   |   |   |   |                     |
| Rust (kokonaisuudessaan) |                   |   |   |   |   |                     |

** Ehdota mistä haluat oppia enemmän (ei pakollinen)

Jos olet esimerkiksi löytäny uuden mielenkiintoisen aiheen ja
haluaisit, että seuraavilla tunneilla käsiteltäisiin sitä, voit
kirjoittaa siitä tähän:

** Avoin palaute (ei pakollinen)

Jos koit opetuksessa olevan parannettavaa tai haluat kertoa
oppimisestasi sanoin, voit kirjoittaa siitä tähän:
