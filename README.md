# [Rust-lukiokurssi](https://otafablab.gitlab.io/rust-lukiokurssi/)


## `mdbook`in käyttö

TODO
- mdbokomermaid init
- dependencies

### Mermaid

Mermaid does not come with the repository, so fetch it manually before building the book

```console
wget https://cdnjs.cloudflare.com/ajax/libs/mermaid/8.6.3/mermaid.min.js
```
