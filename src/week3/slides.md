
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

# Muisti ja lainaus

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Omistajuuden säännöt

- Jokaisella arvolla on tasan yksi omistaja tietyllä hetkellä
- Kun omistaja poistuu skoopista, arvo pudotetaan eikä siihen pääse
  enää käsiksi

<div class="ferris-container">
<img src="https://rustacean.net/assets/rustacean-flat-happy.svg" style=" top: 0; left: 10em" class="ferris">
</div>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

# Kutsupino (stack)

<div class="container"><div class="col-top">

- Pino kehyksiä
- Funktiokutsu työntää kehyksen kutsupinoon ylimmäksi
- Funktion palautuessa ylin kehys tuhotaan
- Kääntäjän pitää tietää kääntöaikana jokaisen kehyksen ja niille
  laitettavan tiedon **koko**

</div><div class="col">

![stack](./stack.svg) <!-- .element: style="position: relative; scale: 1.25; top: -2em" -->

</div></div>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Kutsupinon lautasmalli

<div class="container"><div class="col">

![lautasmalli](./lautasmalli.png)

</div><div class="col">

```
fn add(x: i32) {
	let y = 10;
}

fn main() {
	let x = 5;
	add(x); // <---
}
```

</div></div>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

<div class="container"><div class="col">

![lautasmalli](./lautasmalli-return.png)

</div><div class="col">

```
fn add(x: i32) -> i32 {
	let y = 10;
	x + y // <---
}

fn main() {
	let x = 5;
	let z = add(x);
}
```

</div></div>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Tavut muistissa

<datum class="spaced">
    <name><code>u8</code>, <code>i8</code></name>
    <visual class="bytes">
        <byte><code></code></byte>
    </visual>
</datum>
<datum  class="spaced">
    <name><code>u32</code>, <code>i32</code></name>
    <visual class="bytes">
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
    </visual>
</datum>
<datum  class="spaced">
    <name><code>u64</code>, <code>i64</code></name>
    <visual class="bytes">
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
    </visual>
</datum>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Tavut muistissa

<datum class="spaced">
    <name><code>usize</code>, <code>isize</code></name>
    <visual class="sized">
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
    </visual>
    <description>
        <b>Osoittimen</b> kokoinen<br>(8 tavua 64-bittisellä prosessorilla)
    </description>
</datum>
<datum class="spaced">
<name><code>*const T</code></name>
    <visual class="ptr">
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
        <byte><code></code></byte>
	</visual>
	<description>
		Osoitin <code>ptr</code>
	</description>
</datum>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Tyypit muistissa

<datum class="spaced">
    <name class="nogrow"><code>T</code></name>
    <name class="hidden">x</name>
    <visual>
       <framed class="any t"><code>T</code></framed>
    </visual>
    <description class="nogrow">Kääntöaikana<br>tunnettu koko</description>
</datum>
<datum class="spaced">
    <name><code>[T; n]</code></name>
    <visual>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <note>... <code>n</code></note>
    </visual>
    <description>Taulukko, joka sisältää<br><code>n</code> elementtiä</description>
</datum>
<datum class="spaced">
    <name><code>[T]</code></name>
    <visual>
       <note>...</note>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <note>...</note>
    </visual>
    <description><b>Slice</b> on jatkuva alue muistia ja koostuu<br>vain tyypin <code>T</code> ilmentymistä. Slicen koko<br>tiedetään aina ajonaikana</description>
</datum>

---
<!-- .slide: data-background="hsl(200, 6%, 10%)" -->

# Keko (heap)

- Dynaaminen muisti
- Mahdollistaa vapaakokoisten tyyppien tallentamisen toisin kuin
  pinoon kehykselle
- Hitaampi kuin stack, koska muisti pitää varata
- Osa tietorakenteesta (kuten osoitin) sijaitsee aina stackilla

    <memory-entry class="double">

<datum style="position: relative; left: -10em">
<memory-entry class="double" style="width: 200vw">
<memory class="heap">
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
       <framed class="any t"><code>T</code></framed>
</memory>
</memory-entry>
</datum>

---
<!-- .slide: data-background="hsl(210, 45%, 8%)" -->

<div class="ferris-container">
<img src="https://rustacean.net/assets/rustacean-flat-gesture.svg" style="top: 4em; left: 10em" class="ferris">
</div>

<datum>
    <name><code>Vec&lt;T&gt;</code></name>
    <visual>
        <ptr>
           <code>ptr</code>
        </ptr>
        <sized>
            <code>capacity</code>
        </sized>
        <sized>
            <code>len</code>
        </sized>
    </visual>
    <memory-entry class="double">
        <memory-link style="left:25%;">|</memory-link>
        <memory class="heap capacity">
            <div>
                <framed class="any t"><code>T</code></framed>
                <framed class="any t"><code>T</code></framed>
                <note>... len</note>
            </div>
            <capacity>← <note>capacity</note> →</capacity>
        </memory>
    </memory-entry>
</datum>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Referenssi

<datum class="spaced">
    <name><code>&T</code></name>
    <visual>
        <ptr>
           <code>ptr</code>
        </ptr>
    </visual>
    <memory-entry>
        <memory-link style="left:46%;">|</memory-link>
        <memory class="anymem">
            <framed class="any t"><code>T</code></framed>
        </memory>
    </memory-entry>
    <description>Pitää viitata validiin<br>ilmentymään <code>T</code>stä</description>
</datum>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Use after free

<div class="ferris-container">
<img src="https://doc.rust-lang.org/book/img/ferris/does_not_compile.svg" style="top: 0; left: 10em" class="ferris">
</div>

```rust ,compile_fail
let mut data = vec![1, 2, 3];
// get an internal reference
let x = &data[0];

// OH NO! `push` causes the backing storage
// slice of `data` to be reallocated.
// Dangling pointer! Use after free!
data.push(4);

println!("{}", x);
```

(Yleinen virhe C-ohjelmoinnissa)

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

```rustc
error[E0502]: cannot borrow `data` as mutable
	because it is also borrowed as immutable
  --> src/main.rs:8:5
   |
4  |     let x = &data[0];
   |              ---- immutable borrow occurs here
...
8  |     data.push(4);
   |     ^^^^^^^^^^^^ mutable borrow occurs here
9  |
10 |     println!("{}", x);
   |                    - immutable borrow later used here
```

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" style="height: 100%" -->

```rust
fn main() {
    let s1 = gives_ownership();
    let s2 = String::from("hello");
	
	// s2 is moved into takes_and_gives_back,
	// which also moves its return value into s3.
	// ... in a way s2 was borrowed
    let s3 = takes_and_gives_back(s2);
	// continue using the string ...
}

fn gives_ownership() -> String {
    let some_string = String::from("yours");

	// some_string is returned and
	// moves out to the calling function
    some_string
}

fn takes_and_gives_back(mut a_string: String) -> String {
	// do something with a_string ...
	
    a_string
}
```
<!-- .element: style="height: inherit" -->

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Lainauksen säännöt

<ul>
<li>Jokaisella hetkellä ohjelman suorituksen aikana voi olla...</li>
<ul class="fragment"><li>Mikä tahansa lukumäärä muuttumattomia lainauksia <code>&T</code></li></ul>
<ul class="fragment"><li>Tai yksi muuttuva lainaus <code>&mut T</code></li></ul>

<li class="fragment">

Referenssien tulee aina osoittaa validiin `T`n instanssiin

</li>
</ul>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Otarustlings tauko

<div class="ferris-container">
<img src="https://rustacean.net/assets/corro.svg" style="top: 0; left: 10em" class="ferris">
</div>

<div class="ferris-container">
<img src="https://rustacean.net/assets/rustacean-flat-happy.svg" style="top: 0; left: -5em" class="ferris">
</div>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Referenssi sliceen

<datum class="spaced">
    <name><code>&[T]</code></name>
    <visual>
        <ptr>
           <code>ptr</code>
        </ptr>
        <sized>
           <code>len</code>
        </sized>
    </visual>
    <memory-entry class="double">
        <memory-link style="left:24%; transform: scale(1, 4) translate(0px, 1em); height: 3em; position: relative;">|</memory-link>
        <memory class="anymem">
			<name style="top: -1.4em; left: 0; position: absolute;"><code>[T]</code></name>
            ...
            <framed class="any" style="width: 2.14em;"><code>T</code></framed>
            <framed class="any" style="width: 2.14em;"><code>T</code></framed>
            ...
        </memory>
    </memory-entry>
    <description>Slicet on olemassa vain referenssinä</description>
</datum>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->


## String slice

<datum class="spaced">
    <name><code>&str</code></name>
    <visual>
        <ptr>
			<code>ptr</code>
        </ptr>
        <sized>
            <code>len</code>
        </sized>
    </visual>
    <memory-entry class="double">
        <memory-link style="left:24%;">|</memory-link>
        <memory class="anymem">
            ...
            <byte class="bytes"><code>U</code></byte>
            <byte class="bytes"><code>T</code></byte>
            <byte class="bytes"><code>F</code></byte>
            <byte class="bytes"><code>-</code></byte>
            <byte class="bytes"><code>8</code></byte>
            ...
        </memory>
    </memory-entry>
    <description><b>String slice referenssi</b><br><code>len</code> on pituus tavuina</description>
</datum>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->


# Ohjelma

- Konekielelle käännetyt käskyt (instructions)
- Usein kutsunaan _binääriksi_
- Ladataan muistiin, kun se suoritetaan

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

# Prosessi

<div class="container"><div class="col-top">

- Luodaan ohjelman käynnistyessä
- Omistaa oman muistialueen
- Käyttöjärestelmä käynnistää prosessin
- Prosessissa on aina vähintään yksi _säie_

</div><div class="col">

![htop](./htop.png)
<!-- .element: style="scale: 1.25" -->

</div></div>

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

# Säie

- Suorittaa konekäskyjä itsenäisesti
- Joka säikeellä on oma kutsupino
- Prosessin säikeet jakavat kaikki muut resurssit

---
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

## Lopuksi

<div class="container"><div class="col-top">

- Muistakaa tutustua [lisämateriaaliin](../extra.html)
  - Huijauslehdet ovat käteviä
  - [`explaine.rs`](https://jrvidal.github.io/explaine.rs/)
- Lukekaa [_kirjasta_ kappale 4.1](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html)
- Etsikää lisää tietoa ja kysykää apua

</div><div class="col-top">

![idiomatic rust](./cheatsrs_idiomatic.png)

</div></div>
