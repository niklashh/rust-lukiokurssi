# Viikko 3

## Tavoitteet

- tietää teknisiä yksityiskohtia eri tietorakenteista, kuten `Vec`, `String` ja `&[T]`
- tietää pinon ja keon merkityksen ohjelman toiminnalle
- ymmärtää omistajuuden säännöt
- tietää lainaamisen säännöt
- tietää mitä prosessit ovat ja miten käyttöjärjestelmä liittyy asiaan
- tietää mikä on ohjelmatiedosto ja konekieli
- osaa etsiä tietoa Rustista
