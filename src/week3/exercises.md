# Tehtävät

Lue _kirjasta_ [kappale 4.1](https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html).

## `otarustlings`

Viikon tehtävät tehdään `otarustligs` ohjelmaa käyttäen.

Tehtävät ovat kansiossa `week3`.

`otarustlings`in voit päivittää komennolla:

```console
cargo install --force otarustlings
```

[Lue `otarustlings` ohje](../otarustlings.md)

> Tätä varten tarvitse [Rustin työkalut](../week1/introduction.md#rustin-asennus)
