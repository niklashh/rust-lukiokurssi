# Johdanto

## Tietoa kurssista

Tämän kurssin ovat kehittäneet Niklas Halonen ja Matias
Zwinger. Kehityksessä ovat auttaneet myös Roope Salmi, Jens
Pitkänen ja Tuukka Moilanen.

Kurssin tarkoitus on ensisijaisesti opettaa ohjelmoinnin syvällisiä
konsepteja Rust-ohjelmointikielen avulla. Ohjelmointikielet ovat vain
työkaluja ongelmien ratkaisuun eikä Rust todellakaan ole aina paras
työkalu. Ohjelmistokehittäjän on tärkeämpää ymmärtää miten yleisiä
ohjelmoinnin konsepteja voi käyttää hyödyksi ohjelmointikielestä
riippumatta.

## Tietoa kurssin alustasta

Kurssimateriaali on kirjoitettu
[`mdBook`](https://github.com/rust-lang/mdBook)-alustalla, jonka
lisäksi diaesityksiä ajetaan [`revealjs`](https://revealjs.com/)
paketilla. Kurssimateriaalin lähdekoodi on avointa ja löytyy
[GitLabista](https://gitlab.com/niklashh/rust-lukiokurssi). Kurssin
integroidun kehitysympäristön lähdekoodi löytyy myös
[GitLabista](https://gitlab.com/otafablab/rust-code-server).

## Oppimistavoitteet

Kurssin tavoitteena on oppia seuraavat asiat:

- Ymmärtää ohjelmoinnin peruskonsepteja syvällisemmin:
  - pattern matching
  - muuttuvuus
  - tietorakenteet, kuten `Vec` ja `HashMap`
  - muistinhallinta
  - osoittimet
  - staattinen ja dynaaminen tyypitys
- Osata lukemaan ohjelmointiaineistoa, kuten `rustdoc`-dokumentaatiota ja Rust-lähdekoodia
- Osata itsenäisesti etsimään tietoa ongelman ratkaisemiseksi
- Osata käyttää kehitysympäristöä hyödyksi ohjelman muokkaamisessa ja debuggauksessa
- Ymmärtää sekä manuaalisen, että automaattisen testaamisen käsitteet ja merkityksen ohjelmistokehityksessä
- Ymmärtää siistin lähdekoodin merkityksen
- Osata käyttää ulkoisia sekä itsetekemiä kirjastoja ohjelmissa
- Tietää ohjelmoinnin terminologiaa suomeksi sekä englanniksi:
  - konekieli (machine code)
  - kääntäjä (compiler)
  - pino (stack)
  - keko (heap)
- Osata funktionaalisen ohjelmoinnin paradigman mukaisia konsepteja:
  - Korkeamman asteen funktio
  - Muuttumattomuus (immutability)
  - Geneerisyys
  - Algebrallinen tietotyyppi
- Osata imperatiivisen ohjelmoinnin paradigman mukaisia konsepteja:
  - Käskyt
  - Suorituksen ohjaus ehtolauseilla
  - Tiedon muuttamista paikallaan
- Ymmärtää Rustin tyyppijärjestelmän ero muista ohjelmointikielistä:
  - Periytyminen (inheritance)
  - Piirteet (traits)
  - Nollakustannusabstraktiot ja -tyypit (zero-cost abstractions and types)
- Ymmärtää Rustin omistajuusparadigman hyödyt ja haitat
- Osata kollaboroivasti kehittää toimivaa Rust-lähdekoodia esimerkiksi Gitin avulla

## Pidetyt kurssit

- Otaniemen lukiolla 2022 keväällä (versio 0.5.0)
- Otaniemen lukiolla 2022 syksyllä (versio 1.0)
