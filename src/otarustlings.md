# Tehtäväalusta

Kurssin tehtävät tehdään [`otarustlings` tehtäväalustalla
(docs.rs)](https://docs.rs/otarustlings/latest/otarustlings/).

`otarustlings` tehtäviä ei tarvitse palauttaa kurssin aikana vaan
kaikki [palautetaan vasta
lopussa](#tehtävien-palauttaminen). Bonus-tehtävät (B:llä alkavat)
eivät ole pakollisia vaan niistä saa lisäpisteitä.

> `otarustlings` on Rust kurssia varten käsin uudelleenkirjoitettu
> [`rustlings`](https://github.com/rust-lang/rustlings)
> klooni. `rustlings` kirjoitettiin uudelleen koska se oli niin
> huonosti koodattu. Lopulta `otarustlings` on ihan yhtä huonoa koodia
> mutta ominaisuudet ovat sentään paremmat.

> `otarustlings`in jokainen tehtävä sisältää "testin", vaikka tehtävän
> ratkaisuksi riittää saada koodi kääntymään. Tämä johtuu siitä että
> tulostetut asiat eivät tulisi muuten näkyviin testatessa.

## Asentaminen

Asenna `otarustlings` Cargolla:

```console
cargo install otarustlings
```

> `otarustlings`in pystyy asentamaan manuaalisesti Linux-x86_64
> tietokoneelle lataamalla uusimman binäärin [repositorion
> julkaisuista](https://gitlab.com/otafablab/otarustlings/-/releases)
> (kohdasta **Assets > Other**),

## Quick start

Suorita seuraavat komennot joko erillisellä komentorivillä tai VS
Coden integroidulla komentorivillä

- Asenna `otarustlings` (ei toimi windowsilla vielä)
  ```console
  cargo install otarustlings
  ```
  
- Luo tehtävät
  ```console
  otarustlings init
  ```
  
  > Huomaa: komento ei ylikirjoita vanhoja tiedostoja
- Aloita otarustlings
  ```console
  otarustlings start
  ```
  
- Avaa tehtävät koodieditorissa, ja aloita koodaaminen. Esimerkiski,
  jos käytät VS Codea, voit suorittaa seuraavan komennon samasta
  kansiosta jossa suoritit `otarustlings init`

  ```console
  code exercises/week1
  ```

## Ohje

Käyttöohje löytyy [`otarustlings` paketin
dokumentaatiosta](https://docs.rs/otarustlings/latest/otarustlings/#usage).

`otarustlings`in komentoriviohjeet saat komennolla

```console
otarustlings --help
```

## Rust analyzerin käyttö

[Rust analyzerin](https://rust-analyzer.github.io/) käyttäminen on
ehdottomasti sallittua ja suositeltavaa.

Rust analyzer tarvitsee `Cargo.toml` pakettimanifestitiedoston, jotta
sitä voi käyttää tehtävissä. Kyseinen manifesti sijaitsee sijainnissa
`exercises/Cargo.toml.hack`, joka pitää uudelleennimetä
`Cargo.toml`iksi, jotta Rust analyzer tunnistaa sen.

> Tiedoston nimi ei voinut olla `Cargo.toml`, sillä muuten koko
> `exercises` kansio olisi puuttunut
> julkaisusta. [Tulevaisuudessa](https://gitlab.com/otafablab/otarustlings/-/issues/3)
> `otarustlings` uudelleennimeää tiedoston automaattisesti.

## Tehtävien palauttaminen

Otarustlings tehtäväkansio `exercises` palautetaan kurssin
lopussa. Kansion voi arkistoida esimerkiksi `zip` tai `tar.gz`
muotoon, joka lähetetään arvioitavaksi Niklakselle:
[telegram](https://t.me/niklashh) tai
[sähköposti](mailto:niklas.2.halonen@aalto.fi).

<!-- ### `rust-code-server`istä tehtävien lataaminen -->

<!-- Avaa VS codessa kansio, jossa suoritit `otarustlings init` komennon (jonka -->
<!-- sisällä on `exercises`-kansio). Right clickaa `exercises`-kansiosta ja -->
<!-- paina `Download...` -->

<!-- ![download exercises](./download_rcs.png) -->
