# Viikko 2

Toisen viikon tehtävät tehdään
[`otarustlings`-tehtäväalustalla](../otarustlings.md).

`otarustlings` käyttöohje löytyy [sen docs.rs
sivulta](https://docs.rs/otarustlings#usage)

## Tavoitteet

- ymmärtää muuttujien luomisen
- tietää eri primitiivityyppejä
- tietää syntaksin yleisesti
- tietää mistä etsiä tietoa
- on haju omistajuudesta
