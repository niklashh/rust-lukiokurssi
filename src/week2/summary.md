# Yhteenveto

Toisella viikolla käytiin Rustin syntaksia ja pääpiirteitä läpi.

Uusia asioita olivat:

- tyypit
- skooppi
- funktioiden syntaksi
- omistajuus ja lainaaminen
- Rustin erilaisuus muihin kieliin nähden

Vanhoja asioita joita kerrattiin olivat:

- muuttujat
- `cargo new`, `cargo run`
- komentorivin käyttö

---

## Harjoitustunti

- viikko 1 palaute
- jotain lisäihmettelyä skoopista
- ajonaika vs kääntöaika
- paniikki
- ajonaikana ei voi tulla tyyppivirhettä
- stringin metodit, &mut String
- literaalit, string literal vs Strinh
- static tuple godbolt

---

### Jotain lisäihmettelyä skoopista

```rust
let a = 1;
let a = 2;
{
    let mut a = 3;
    dbg!(a);
    for _ in 0..4 {
        a += 1;
        dbg!(a);
        let a = "kana";
        dbg!(a);
    }
    dbg!(a);
}
dbg!(a);
```

---

### Ajonaika vs kääntöaika

```rust
const A: i32 = 5;
```

```rust
const fn add(a: i32, b: i32) -> i32 {
    a + b // + is const fn
}

const A: i32 = add(5, 5);

fn main() {
    dbg!(A);
}
```

---

### Ajonaika vs kääntöaika

```rust ,compile_fail
const FACTS: String = String::from("Rust > Python");
```

---

### Ajonaika vs kääntöaika

```rust
const FACTS: &'static str = "Rust > Python";
```

---

## Viikkopalaute

Joka viikolla voi ja kannattaa täyttää palautelomake, jonka avulla
opettajat voi päätellä mitä pitäisi opettaa enemmän

[Anna palautetta miten sinulla meni tämän viikon aiheet](https://docs.google.com/forms/d/e/1FAIpQLSfytfAJDf2Un4QxErUzNiaujWdGjN4NjmfKPqUbLAVjhfi9pA/viewform?usp=sf_link)
