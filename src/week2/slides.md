
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

# Muuttujat ja tyypit

---

- Kurssimateriaali

## `doc.fablab.rip`

- Liittykää Telegram:

## `lyli.fi/rust`

<!-- https://lyli.fi/poista-lyhennys/rust/GScx47VCWpyFG18bsdx3DxayHIWL4nmIjB6lg4Sd -->

---

## `let`

```rust
use std::io;

fn main() {
    println!("Guess the number!");

    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}
```

---

```rust
use std::io;

fn main() {
    // ...

    let mut guess = String::new();

    io::stdin();

    // ...
}
```

```python
from std import io

def main():
    # ...

    guess = String()

    io.stdin()

    # ...
```

---

## `use`

```rust
// use std::io;

fn main() {
    // ...

    let mut guess = String::new();

    std::io::stdin();

    // ...
}
```

---

## `mut`

<div class="ferris-container">
<img src="https://doc.rust-lang.org/book/img/ferris/does_not_compile.svg" style=" top: 0; left: 10em" class="ferris">
</div>

```rust ,compile_fail
let guess = String::new();

std::io::stdin()
    .read_line(&mut guess)
    .expect("Failed to read line");
```

---

```rustc
error[E0596]: cannot borrow `guess` as mutable, as it is not declared as mutable
 --> a.rs:5:20
  |
2 |     let guess = String::new();
  |         ----- help: consider changing this to be mutable: `mut guess`
...
5 |         .read_line(&mut guess)
  |                    ^^^^^^^^^^ cannot borrow as mutable

error: aborting due to previous error
```

---

## `fn`

```rust
fn hello(name: &str) {
    println!("Hello, {}", name);
}
```

---

```rust
fn bad_max(x: i32, y: i32) -> i32 {
    let maximum;
    if x > y {
        maximum = x;
    } else {
        maximum = y;
    }
    return maximum;
}

fn better_max(x: i32, y: i32) -> i32 {
    let maximum = if x > y {
        x
    } else {
        y
    };
    maximum
}
```
<!-- .element: style="height:23em" -->

---

# `if`-lauseke

<div class="ferris-container">
<img src="https://rustacean.net/assets/rustacean-flat-happy.svg" style=" top: 0; left: 10em" class="ferris">
</div>

```rust
fn best_max(x: i32, y: i32) -> i32 {
    if x > y { x } else { y }
}
```

---

# Scope

<div class="ferris-container">
<img src="https://doc.rust-lang.org/book/img/ferris/does_not_compile.svg" style=" top: 0; left: 10em" class="ferris">
</div>

```rust ,compile_fail
let x = 5;
let y = 3;

if x > y {
    let diff = x - y;
}
println!("{}", diff);
```

---

# Scope

```rustc
error[E0425]: cannot find value `diff` in this scope
 --> /tmp/mdbook-zzWDbB/week2/slides.md:164:16
  |
9 | println!("{}", diff);
  |                ^^^^ not found in this scope

error: aborting due to previous error
```

---

## Scope ratkaisu?

<div class="ferris-container">
<img src="https://doc.rust-lang.org/book/img/ferris/does_not_compile.svg" style=" top: 0; left: 10em" class="ferris">
</div>

```rust ,compile_fail
let x = 5;
let y = 3;

let mut diff;
if x > y {
    diff = x - y;
}
println!("{}", diff);
```

---

## Scope ratkaisu?

```rustc
error[E0381]: borrow of possibly-uninitialized variable: `diff`
  --> /tmp/mdbook-HR2wgB/week2/slides.md:198:16
   |
10 | println!("{}", diff);
   |                ^^^^ use of possibly-uninitialized `diff`
   |

error: aborting due to previous error
```

---

```rust ,ignore
let x = 5;
{               // näkyvissä: x
    let y = 3;
    {           // näkyvissä: x, y
        if x > y {
            let diff = x - y;
            {   // näkyvissä: x, y, diff
                println!("{}", diff);
            }
        }       // diff pudotetaa
    }
}               // y pudotetaan
```

---

# Omistajuus

<div class="ferris-container">
<img src="https://doc.rust-lang.org/book/img/ferris/does_not_compile.svg" style=" top: 0; left: 10em" class="ferris">
</div>

```rust ,compile_fail
let x = String::from("hello");
let y = x;
println!("{}", x);
```

---

```rustc
error[E0382]: borrow of moved value: `x`
 --> /tmp/mdbook-7pr3zz/week2/slides.md:208:16
  |
3 | let x = String::from("hello");
  |     - move occurs because `x` has type `String`,
  |       which does not implement the `Copy` trait
4 | let y = x;
  |         - value moved here
5 | println!("{}", x);
  |                ^ value borrowed here after move

error: aborting due to previous error
```

---

# Lainaus

```rust
let mut s = String::from("hello");
// push_str lainaa `s` mutablena
s.push_str(", world!");
println!("{}", s);
```

- [`push_str` doku](https://doc.rust-lang.org/std/string/struct.String.html#method.push_str)

---

## Otarustlings
<!-- .slide: data-background="#420" -->

```console
cargo install otarustlings # might take a while
cd Desktop
otarustlings init
```

![bongo](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.tenor.com%2F5Bjheb4fKhMAAAAj%2Fferris-bongo.gif&f=1&nofb=1&ipt=9ec9f9e1e176e94405b628cbe19017829069244984f92815cd4ae7e5618771af&ipo=images)
