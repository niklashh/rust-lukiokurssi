# Tehtävät

Tehtävät tehdään `otarustligs` ohjelmaa käyttäen.

Toisen viikon tehtävät ovat kansiossa `week2`.

[Lue `otarustlings` ohje](../otarustlings.md)

> Tätä varten tarvitse [Rustin työkalut](../week1/introduction.md#rustin-asennus)
