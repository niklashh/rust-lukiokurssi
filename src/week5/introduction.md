# Viikko 5 (keskeneräinen)

Tällä viikolla käsitellään lähdekoodia, virheenkäsittelyä, geneerisiä tyyppejä ja piirteitä.

## Tavoitteet

- tietää **semanttisen versioinnin** perusteet
- tietää mitä on **avoin** lähdekoodi, sekä mitä avointa lähdekoodia omalla tietokoneella pyörii
- tietää **moduulihierarkia** ja itemien **näkyvyys**
- osata käyttää eri Rust-kirjastoja omissa ohjelmissa ja **kirjastoissa**
- tietää `Result`-tyypin toiminta
  - miten virheenkäsittely on toteutettu Rustissa (ja funktionaalisissa kielissä)
  - miten virheenkäsittely on toteutettu muissa imperatiivisissa kielissä
- tietää mitä **panikointi** tarkoittaa ja miten sitä voi välttää
- tietää **geneeristen** tyyppien syntaksi
- tietää **piirteiden** toiminta
  - tietää `Iterator`-piirteen määritelmä sekä tyyppejä jotka implementoivat `Iterator`-piirteen
- osata implementoida omia **tietorakenteita**, kuten binääripuu
