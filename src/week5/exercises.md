# Tehtävät

- Lue _kirjasta_
[kappale 9.1](https://doc.rust-lang.org/book/ch09-01-unrecoverable-errors-with-panic.html)
- Lue _kirjasta_
[kappale 9.2](https://doc.rust-lang.org/book/ch09-02-recoverable-errors-with-result.html)

## `otarustlings`

Viikon tehtävät tehdään `otarustligs` ohjelmaa käyttäen.

Tehtävät ovat kansiossa `week5`.

> **Varoitus:** tehtävä `14-bst` on haastava ja pitkä. Tekemällä tehtävän olet ansainnut yhden ilmaisen lounaan opettajan kanssa.

`otarustlings`in voit päivittää komennolla:

```console
cargo install --force otarustlings
```

[Lue `otarustlings` ohje](../otarustlings.md)

> Tätä varten tarvitse [Rustin työkalut](../week1/introduction.md#rustin-asennus)

