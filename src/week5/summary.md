# Yhteenveto

Tällä viikolla opittiin lähdekoodin rakenteesta, tyyppien välisistä rajapinnoista (piirteet), virheenkäsittelystä geneerisen `Result`-enumin avulla.

Uusia asioita olivat:

- moduulit
- paketin konfiguraatio `Cargo.toml`
- virheenkäsittely muissa (imperatiivisissa) kielissä
- summatyypit virheenkäsittelyssä, `Result`, funktionaalinen virheenkäsittely, `?`
- virheiden propagointi ja "vahvan" tyyppijärjestelmän edut
- kirjastot ohjelmien rinnalla
- geneeriset, piirteet
- `trait Iterator`, adapterit
- trait boundien perusteet

Vanhoja asioita joita kerrattiin olivat:

- `()` ja `None` ero muiden kielten `null / nil / undefined` arvoihin
- `Option` virheenkäsittelyssä
- esimerkkejä iteraattoreista
- `map`

## Viikkopalaute

Joka viikolla voi ja kannattaa täyttää palautelomake, jonka avulla
opettajat voi päätellä mitä pitäisi opettaa enemmän

[Anna palautetta miten sinulla meni tämän viikon aiheet](https://docs.google.com/forms/d/e/1FAIpQLSekY1qpnAjCcYstTH3y9C8rDKwQ5yEosJWi8oEt58hJMN6jGg/viewform?usp=sf_link)
