
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

<style>
table { border-collapse: collapse; }
tr { border-bottom: none; }
tr:nth-child(even) {background-color: hsl(210, 25%, 11%);}
td,th {border-bottom: 1px solid hsl(0, 0%, 21%) !important;}
blockquote {font-style:normal !important;}
</style>

# Rajapinnat ja virheet

> - Versiointi ja lähdekoodi
> - Virheenkäsittely
> - Piirteet
> - Iteraattorit

---

## Semanttinen versiointi

<div class="container"><div class="col">

- Kertoo kirjastojen versioiden yhteensopivuuden
- `MAJOR.MINOR.PATCH`
- Taaksepäinyhteensopivuus (backward compatibility)

</div><div class="col">

![semver](./semver.png) <!-- .element: style="scale: 90%" -->

</div></div>

---

## Avoin lähdekoodi

<div class="container"><div style="min-width:65%" class="col">

- Ilmaista ja vapaasti saatavilla
- Lähdekoodia saa jakaa ja siihen saa tehdä muutoksia
- Kaikki netistä, kuten GitHubista, löytyvä lähdekoodi ei ole avointa
- Lisenssit: GPL, MIT, Apache, BSD, UNLICENSE
- Ohjelmisto: Chromium, VSCodium, GNU/Linux, Firefox, GNU Emacs

</div><div class="col">
<div style="display: grid; grid-template-columns: auto auto auto; margin-left: 1em;">

![chromium](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Chromium_Logo.png/640px-Chromium_Logo.png)

![linux](https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg)

![GNU](https://upload.wikimedia.org/wikipedia/en/2/22/Heckert_GNU_white.svg)

![firefox](https://upload.wikimedia.org/wikipedia/commons/a/a0/Firefox_logo%2C_2019.svg)

![blender](https://upload.wikimedia.org/wikipedia/commons/0/0c/Blender_logo_no_text.svg)

![emacs](https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/EmacsIcon.svg/1024px-EmacsIcon.svg.png)

![vscodium](https://upload.wikimedia.org/wikipedia/commons/5/56/VSCodium_Logo.png)

![debian](https://upload.wikimedia.org/wikipedia/commons/4/4a/Debian-OpenLogo.svg) <!-- .element: style="filter:invert(1) hue-rotate(200deg) saturate(10)" -->

![freeCAD](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/FreeCAD-logo.svg/1024px-FreeCAD-logo.svg.png)

</div>
</div></div>

---

## Moduulit

<div class="container"><div style="min-width:55%" class="col">

- Rust-lähdekoodin voi jakaa tiedostoihin ja kansioihin
  - Jokainen tiedosto ja kansio määrittelee moduulin
- Moduuleilla erotetaan kirjastojen osat
- Moduuli koostuu yksityisistä ja julkisista (`pub`) itemeistä

</div><div class="col-top">

```console
otarustlings/
├── README.md
├── Cargo.toml
└── src/
    ├── lib/
    │   ├── exercise.rs
    │   ├── lib.rs
    │   ├── menu.rs
    │   ├── state.rs
    │   ├── tester.rs
    │   └── utils.rs
    └── main.rs
```

</div></div>

---

## Rust lähdekoodi

<div class="container"><div style="min-width:55%" class="col">

- Paketti koostuu laatikoista
  - `Cargo.toml`
- Laatikko koostuu moduuleista
- Laatikko voi olla ohjelma (bin) tai kirjasto (lib)

![crates](https://crates.io/assets/Cargo-Logo-Small.png) <!-- .element style="margin-top:-1em" -->

</div><div class="col-top">

```toml
[package]
name = "otarustlings"
version = "0.4.4"
edition = "2021"

[lib]
name = "otarustlings"
path = "src/lib/lib.rs"

[[bin]]
name = "otarustlings"
path = "src/main.rs"
doc = false

[dependencies]
notify = "4.0.17"
walkdir = "2.3.2"
toml = "0.5.8"
```
<!-- .element: style="height:23em" -->

</div></div>

---

## Pakettihierarkia

<div class="container"><div class="col">

- Paketeilla on riippuvuuksia toisten pakettien kirjastoihin (lib)
- Kääntöaika ∝ pakettien määrään
- Yhdellä paketilla voi olla yli 100 _transienttia_ riippuvuutta

</div><div class="col-top">

```console
λ cargo tree
otarustlings v0.4.4
├── notify v4.0.17
│   ├── bitflags v1.3.2
│   ├── inotify v0.7.1
│   │   ├── bitflags v1.3.2
│   │   ├── inotify-sys v0.1.5
│   │   │   └── libc v0.2.124
│   │   └── libc v0.2.124
│   ├── libc v0.2.124
│   ├── mio v0.6.23
│   │   ├── cfg-if v0.1.10
│   │   ├── iovec v0.1.4
│   │   │   └── libc v0.2.124
│   │   ├── libc v0.2.124
```
<!-- .element: style="height: 75%" -->

</div></div>

---

## Virheenkäsittely

- Bugi, Virhe, `Result`
- Virheenkäsittely on eksplisiittistä ja pakollista
- Paniikki `panic!`, `unwrap`
- Propagaatio `?`

```rust ,ignore
fn read_hello() -> Result<String, io::Error> {
    let mut file = File::open("hello.txt")?;
    let mut username = String::new();
    file.read_to_string(&mut username)?;
    Ok(username)
}
```

---

## Virheenkäsittely Pythonissa

```python
def upper(name):
	if name == "":
        raise Exception("no name")
	return name.upper()

def main():
	try:
        name = upper("sammy")
    except Exception as err:
		print("Could not upper:", err)
		return

	print("Uppercase name:", name)
```

---

## Virheenkäsittely GO:ssa

```go
func upper(name string) (string, error) {
	if name == "" {
		return "", errors.New("no name")
	}
	return strings.ToTitle(name), nil
}

func main() {
	name, err := upper("sammy")
	if err != nil {
		fmt.Println("Could not upper:", err)
        return
	}

	fmt.Println("Uppercase name:", name)
}
```
<!-- .element: style="height: 21em" -->

---

## Algebrallinen ratkaisu

<div class="container"><div class="col-top">

**Imperatiivisissa kielissä**

- Virheet keskeyttävät funktion suorituksen
- `NULL` on mitä tahansa tyyppiä
- `NULL`in tarkistaminen ei ole pakotettua
- Ei ole selvää millä rivillä voi tapahtua virhe

</div><div class="col-top">

**Funktionalisissa kielissä**
- Virheet eivät ohita suoritusjärjestystä
- `NULL`ia vastaava arvo ei ohita tyyppijärjestelmää
- `Option` pitää aina tarkistaa (esim `match`) jotta saa sisäisen arvon
- [NULL blog](https://www.lucidchart.com/techblog/2015/08/31/the-worst-mistake-of-computer-science/)

</div></div>

---

| Language    | NULL                   | Maybe                                | NULL Score |
|-------------|------------------------|--------------------------------------|:----------:|
| Objective C | nil, Nil, NULL, NSNull | Maybe, from SVMaybe                  | ⭐         |
| C           | NULL                   |                                      | ⭐⭐       |
| Go          | nil                    |                                      | ⭐⭐       |
| C++         | NULL                   | boost::optional, from Boost.Optional | ⭐⭐⭐     |
| Python      | None                   | Maybe, from PyMonad                  | ⭐⭐⭐     |
| Java        | null                   | java.lang.Optional                   | ⭐⭐⭐⭐   |
| Scala       | null                   | scala.Option                         | ⭐⭐⭐⭐   |
| OCaml       |                        | option                               | ⭐⭐⭐⭐⭐ |
| Rust        |                        | Option                               | ⭐⭐⭐⭐⭐ |
| Haskell     |                        | Maybe                                | ⭐⭐⭐⭐⭐ |
<!-- .element: style="font-size:0.6em" -->

---

<div class="container"><div class="col-top">

#### Ohjelman (bin) virheenkäsittely

- Väärän tilan välttäminen
- Turvallinen kaatuminen
- Käyttäjäystävällisyys

</div><div class="col-top" style="margin-left:1em">

#### Kirjaston (lib) virheenkäsittely

- Väärän tilan välttäminen
- Turvallinen kaatuminen
- Virheiden propagointi
- Totaalit funktiot
- Koodariystävällinen

</div></div>

---

## `Result::<T, E>::unwrap`

```rust ,ignore
impl<T, E> Result<T, E> {
    pub fn unwrap(self) -> T
    where
        E: fmt::Debug,
    {
        match self {
            Ok(t) => t,
            Err(e) => panic!(
				"called `Result::unwrap()` on an `Err` value: {:?}",
				e,
			),
        }
    }
}
```

---

## Geneeriset tyypit

```rust ,ignore
struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

fn main() {
    let p = Point { x: 5, y: 10 };

    println!("p.x = {}", p.x());
}
```
<!-- .element: style="height:20em" -->

---

## `type`

```rust ,ignore
use std::result;

pub type Result<T> = result::Result<T, ConfigError>;

pub fn config_file(path: impl AsRef<Path>) -> Result<Config> { .. }
```
<!-- .element: style="width:100%" -->

- Tapa _nimetä_ jokin tyyppi, kuten "vakio" tyyppitasolla
- Geneerinen tyyppi on kuin funktio tyyppitasolla

---

<div class="container"><div class="col-top" style="min-width:60%">

## Piirteet

- Tyyppien välinen rajapinta
- Kuvaa toiminnallisuutta abstraktisti
- Yleisiä piirteitä:
  - Default
  - Clone
  - Iterator
- Geneeristen tyyppien rajoittaminen
- `dyn Trait`

</div><div class="col-top">

`struct String`

![](./traits.png)

</div></div>

---

## `fn upper`

```rust
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum UpperError {
    NoName,
}

pub fn upper<T>(name: T) -> Result<String, UpperError>
where
    T: AsRef<str>,
{
    let name = name.as_ref(); // convert to &str
    if name.is_empty() {
        return Err(UpperError::NoName);
    }
    Ok(name.to_uppercase())
}

fn main() -> Result<(), UpperError> {
    let name = upper("sammy")?;
    println!("Uppercase name: {}", name);
    Ok(())
}
```
<!-- .element: style="height:26em" -->

---

## `trait Iterator`

```rust ,ignore
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;
}
```

- Abstrakti rajapinta asioille joilta voi pyytää seuraavan arvon
- `Option` ilmaisee onko seuraavaa arvoa
- Loppumaton _laiska_ iterointi
- Ei tule sekoittaa `IntoIterator` piirteen kanssa

---

```rust ,ignore
// src/core/slice/mod.rs
impl<T> [T] {
    pub fn iter(&self) -> Iter<'_, T> {
        Iter::new(self)
    }
}
```

- Mitä tyyppiä on `&self`?

```rust
let x = &[1, 4];
let mut iterator = x.iter();

assert_eq!(iterator.next(), Some(&1));
assert_eq!(iterator.next(), Some(&4));
assert_eq!(iterator.next(), None);
```

- Iteraattori palauttaa `&T` arvoja
- Miksi `mut`?

---

## `struct Iter` (slice)

- `Iter`in kenttiä ja _implementaatiota_ ei tarvitse tietää
- Implementoi `Iterator` piirteen

![](./Iter_impl.png)

---

## Muita `Iter`-nimisiä tietueita

![](./different_iters.png)
<!-- .element: style="height:12em" -->

---

## `std::iter::repeat`

- `Repeat` on `Iterator`
- Palauttaa `next()` kutsuessa kloonin: `Some(elt.clone())`

```rust ,ignore
pub fn repeat<T>(elt: T) -> Repeat<T>
where
    T: Clone,
```

```rust
// the number four 4ever:
let mut fours = std::iter::repeat(4);

assert_eq!(Some(4), fours.next());
assert_eq!(Some(4), fours.next());
assert_eq!(Some(4), fours.next());
assert_eq!(Some(4), fours.next());

// yup, still four
assert_eq!(Some(4), fours.next());
```

---

### Iteraattoriadapterit

|    Metodi | Kuvaus                                                                         |
|----------:|--------------------------------------------------------------------------------|
|     `map` | Palauttaa iteraattorin, jonka alkiot kuvataa `λ`-funktiolla                    |
|  `filter` | Palauttaa iteraattorin, jonka alkiot ovat ne jotka `λ`-funktio kuvaa `true`ksi |
| `collect` | Kuluttaa iteraattorin ja kerää arvot kokoelmaan                                |
<!-- .element: style="font-size:0.6em" -->

```rust ,ignore
fn is_prime(x: &i32) -> bool { .. }

let a = [1, 2, 3, 4, 5, 6];

let mersenne_primes: Vec<i32> = a
    .iter()
    .map(|&x| 2i32.pow(x) - 1)
    .filter(is_prime)
    .collect();

assert_eq!(mersenne_primes, vec![3, 7, 31]);
```

---

## Range iteraattori

```rust ,ignore
fn is_prime(x: &i32) -> bool { .. }

let mut mersenne_primes = (1..)
    .map(|x| 2i32.pow(x) - 1)
    .filter(is_prime);

assert_eq!(mersenne_primes.next(), Some(3));
assert_eq!(mersenne_primes.next(), Some(7));
assert_eq!(mersenne_primes.next(), Some(31));
assert_eq!(mersenne_primes.next(), Some(127));
// ...
assert_eq!(mersenne_primes.next(), Some(618970019642690137449562111)
```

---

## Kotiläksy

- _kirjasta_ 9.1, 9.2
- https://desmondwillowbrook.github.io/blog/rust-error-handling/
  - Käy läpi piirteitä, `Box<dyn Error>`, serde, virheen propagointia

---

---

<!-- lähde: https://desmondwillowbrook.github.io/blog/rust-error-handling/ -->

> ##### `Cargo.toml`
>
> ```toml
> [dependencies]
> serde_json = "1.0.81"
> 
> [dependencies.serde]
> version = "1.0.137"
> features = ["derive"]
> ```

> ##### `lib.rs`
> 
> ```rust ,ignore
> fn read_file(path: &Path) ->
>     Result<Vec<u8>, std::io::Error>
> {
>     let mut f = File::open(&path)?;
>     let mut buf = Vec::new();
>     f.read_to_end(&mut buf)?;
>     Ok(buf)
> }
> ```
> 

---

## `serde`

```rust ,ignore
#[derive(Deserialize)]
pub struct Config {
  pub name: String
}
```

---

```rust, ignore
#[derive(Debug)]
pub enum ConfigError {
    FileError(io::Error),
    DeserializeError(serde_json::Error)
}

pub fn config_file(path: impl AsRef<Path>) ->
	Result<Config, ConfigError>
{
    let buf = read_file(path.as_ref())?;
    let conf = serde_json::from_slice(&buf)?;
    Ok(conf)
}

pub fn config_file<P: AsRef<Path>>(path: P) ->
	Result<Config, ConfigError> // ...
```
<!-- .element: style="height: 20em" -->

---

```rust ,ignore
#[derive(Debug)]
pub enum ConfigError {
    FileError(io::Error),
    DeserializeError(serde_json::Error),
}

impl From<serde_json::Error> for ConfigError {
    fn from(e: serde_json::Error) -> Self {
        Self::DeserializeError(e)
    }
}

impl From<io::Error> for ConfigError {
    fn from(e: io::Error) -> Self {
        Self::FileError(e)
    }
}
```
<!-- .element: style="height: 21em" -->

---

>
> `main.rs`:
> 
> ```rust ,ignore
> use error_handling::config_file;
> 
> fn main () {
> 	let conf = config_file("config.json").unwrap();
> 	println!("{}", conf.name);
> }
> ```
>
<!-- .element: style="width: 20em" -->

---

```text
λ cargo run
thread 'main' panicked at
called Result::unwrap() on an Err value:
	FileError(Os {
		code: 2,
		kind: NotFound,
		message: "No such file or directory"
	})
	src/main.rs:4:43
run with RUST_BACKTRACE=1 to display a backtrace
```

- tiedostoa `config.json` ei ole
- virhe ei kerro tiedoston nimestä

---

```rust ,ignore
# use std::path::PathBuf;
#[derive(Debug)]
pub enum ConfigErrorType {
    FileError(std::io::Error),
    DeserializeError(serde_json::Error),
}

#[derive(Debug)]
pub struct ConfigError { // uusi
    pub path: PathBuf,
    pub err: ConfigErrorType,
}
```
