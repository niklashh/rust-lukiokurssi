# Esimerkkiratkaisut

## 01-tuples

```rust
//! Use a tuple index operator to access the second element of
//! `numbers`.

#[test]
fn indexing_tuple() {
    let numbers = (1, 2, 3);
    // Create a variable holding the second number of the tuple.
    let second = numbers.1;

    assert_eq!(2, second, "This is not the second number in the tuple!")
}
```
