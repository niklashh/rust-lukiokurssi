# Tehtävät

Lue _kirjasta_ [kappale TODO](TODO).

## `otarustlings`

Viikon tehtävät tehdään `otarustligs` ohjelmaa käyttäen.

Tehtävät ovat kansiossa `TODO`.

`otarustlings`in voit päivittää komennolla:

```console
cargo install --force otarustlings
```

[Lue `otarustlings` ohje](../otarustlings.md)

> Tätä varten tarvitse [Rustin työkalut](../week1/introduction.md#rustin-asennus)

