# Yhteenveto

Tällä viikolla opittiin tietotyypeistä, staattisesta tyypityksestä ja algebrallisten tyyppien eduista.

Uusia asioita olivat:

- tietotyyppien luominen
- tyyppiteoria
- algebrallinen tietotyyppi
- funktionaalisen virheenkäsittely perusteet, `Option` virheenkäsittelyssä
- Miksi `None` ei ole sama kuin muiden kielten `null`
- `match`in käyttö ja `if let`
- polymorfismin ja piirteiden perusteet

Vanhoja asioita joita kerrattiin olivat:

- `Option`in käyttöä
- tietotyyppien representaatioita
- olioita

## Viikkopalaute

Joka viikolla voi ja kannattaa täyttää palautelomake, jonka avulla
opettajat voi päätellä mitä pitäisi opettaa enemmän

[Anna palautetta miten sinulla meni tämän viikon
aiheet](https://docs.google.com/forms/d/e/1FAIpQLSf1J2KCNq0gQCCWEUAy5cFfXmthZdIMCWuMe1r4Ezf3s0Fsgg/viewform?usp=sf_link)
