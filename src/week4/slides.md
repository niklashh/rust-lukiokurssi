
<!-- .slide: data-background="hsl(180, 22%, 8%)" -->

# Algebrallinen tyyppiteoria

<img src="https://blog.rachelbarrett.co.uk/wp-content/uploads/2020/08/The-category-of-types.png" style="filter: invert(100%)">

---
<!-- .slide: data-background="hsl(180, 22%, 8%)" -->

## Lainauksen säännöt

<ul>
<li>Jokaisella hetkellä ohjelman suorituksen aikana voi olla <code>&T</code> ja <code>&mut T</code> lainauksia...</li>
<ul class="fragment"><li>Mikä tahansa lukumäärä muuttumattomia lainauksia <code>&T</code></li></ul>
<ul class="fragment"><li>Tai yksi muuttuva lainaus <code>&mut T</code></li></ul>

<li class="fragment">

Referenssien tulee aina osoittaa validiin `T`n instanssiin

</li>
</ul>

---
<!-- .slide: data-background="hsl(180, 22%, 8%)" -->

## Mikä on tyyppi?

<div class="container"><div class="col">

- Tieto luokitellaan tyyppeihin `[i32; 5]`
- Myös funktioilla on tyyppi `fn(String) -> usize`
- Tyyppi määrittelee ja rajoittaa missä muodossa tieto esiintyy
- _Esitys_ muistissa
- Miten funktio on esitetty muistissa?

</div><div class="col-top" style="max-width: 35%">

<br>
<datum class="spaced" style="margin-left: 0">
	<name><code>[i32; 5]</code></name>
    <visual>
       <byte><code></code></byte>
       <byte><code></code></byte>
       <byte><code></code></byte>
       <byte><code></code></byte>
       <byte><code></code></byte>
    </visual>
</datum>

</div></div>

---
<!-- .slide: data-background="hsl(180, 22%, 8%)" -->

## _Algebralliset_ tietotyypit

<div class="container"><div class="col">

- Tulo- ja summatyypit
- Ergonominen `match`
- Konkreetti eikä abstrakti
- Tietoon pääsee helposti käsiksi,<br>koska tiellä ei ole turhia abstraktioita
  - Vertaa _luokkiin_ ja _olioihin_

</div><div class="col" style="scale: 1.5; margin-left: 1em; margin-top: -3em">

<img src="https://blog.rachelbarrett.co.uk/wp-content/uploads/2020/08/tagged-product-and-tagged-union.png" style="filter: invert(100%) contrast(50%)">

</div></div>

---
<!-- .slide: data-background="hsl(180, 22%, 8%)" -->

## Tulotyyppi

`T = bool × i32 × Vec<f64>`

- Tulotyypit sisältävät monta ilmentymää kerralla
  - `T`:n konstruktoiminen vaatii ilmentymän `bool`ista, `i32`sta ja
    `Vec<f64>`sta
- Tuplet ja structit ovat tulotyyppejä

---
<!-- .slide: data-background="hsl(180, 22%, 8%)" -->

## Summatyyppi

`S = A(i32) + B(String)`

- Ilmentymän **variantit** ovat samaa tyyppiä
- Varianteilla on uniikki nimi
  - A ja B
- Enumit ovat summatyyppejä
- `S` ei voi sisältää `i32` ja `String` samaan aikaan

---
<!-- .slide: data-background="hsl(210, 22%, 8%)" -->

## `struct`

```rust
struct Person {
	age: i32,
	name: String,
}
```

---
<!-- .slide: data-background="hsl(210, 22%, 8%)" -->

### Erilaisia `struct`eja

```rust
/// tuple struct
struct S(i32, String);
```

```rust
/// unit struct
struct S;
```

---
<!-- .slide: data-background="hsl(210, 22%, 8%)" -->

## `enum`

```rust
enum E {
  None,
  Some(i32),
  Person { name: String, age: i32 },
}
```

---
<!-- .slide: data-background="hsl(210, 22%, 8%)" -->

## `struct` muistissa

<datum class="spaced">
    <name><code>struct S { b: B, c: C }</code></name>
    <name><code>struct S(B, C)</code></name>
	<br>
    <visual>
       <framed class="any" style="width: 5em"><code>C</code></framed>
       <pad><code>↦</code></pad>
       <framed class="any" style="width: 9em"><code>B</code></framed>
    </visual>
</datum>

---
<!-- .slide: data-background="hsl(210, 22%, 8%)" -->

## `enum` muistissa

<div class="ferris-container">
<img src="https://rustacean.net/assets/rustacean-flat-gesture.svg" style="top: 1em; left: 9em" class="ferris">
</div>

<datum class="spaced">
    <name><code>enum E { A, B, C }</code></name>
	<br>
    <visual class="enum" style="text-align: left;">
        <pad><code>Tag "A"</code></pad>
        <framed class="any">
            <code>A</code>
        </framed>
    </visual>
    <andor>tai</andor>
    <visual class="enum" style="text-align: left;">
        <pad><code>Tag "B"</code></pad>
        <framed class="any" style="width: 6em">
            <code>B</code>
        </framed>
    </visual>
    <andor>tai</andor>
    <visual class="enum" style="text-align: left;">
        <pad><code>Tag "C"</code></pad>
        <framed class="any" style="width: 4em">
            <code>C</code>
        </framed>
    </visual>
</datum>

---
<!-- .slide: data-background="hsl(50, 5%, 10%)" -->

## Missä `None`?

```python
yobs = {"Python": 1989, "Rust": 2010, "Prolog": 1972}
rust_birth = yobs.get("Rust") # 2010
print(rust_birth + 12)        # 2022

java_birth = yobs.get("Java") # None
print(java_birth + 27)        # Wat?
```

---
<!-- .slide: data-background="hsl(50, 5%, 10%)" -->

<div class="ferris-container">
<img src="https://doc.rust-lang.org/stable/book/img/ferris/panics.svg" style="top: -2em; left: 9em" class="ferris">
</div>

```python
java_birth = yobs.get("Java") # None
print(java_birth + 27)        # ERROR!!!

TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'
```

---
<!-- .slide: data-background="hsl(50, 5%, 10%)" -->

```rust
use std::collections::HashMap;

let yobs = HashMap::from([
    ("Python", 1989),
    ("Rust", 2010),
    ("Prolog", 1972),
]);

let rust_birth = yobs.get("Rust");
println!("{:?}", rust_birth); // Some(2010)
```

---
<!-- .slide: data-background="hsl(50, 5%, 10%)" -->

## `HashMap::<K, V>::get`

`pub fn get(&self, k: &K) -> Option<&V>`

---
<!-- .slide: data-background="hsl(50, 5%, 10%)" -->

```rust ,ignore
let java_birth: Option<&i32> = yobs.get("Java"); // None
println!("{:?}", java_birth.map(|y| y + 12));    // None
```

---
<!-- .slide: data-background="hsl(50, 5%, 10%)" -->

### `Option<T>`

```rust
enum Option<T> {
	Some(T),
	None,
}
```

> Variantit eivät ole tyyppejä!

---
<!-- .slide: data-background="hsl(20, 5%, 10%)" -->

### `match`

```rust
enum E {
	/// Unit variant
    None,
	/// Newtype variant
    Some(i32),
	/// Struct variant
    Person { name: String, age: i32 },
}

let e = E::Some(5);
match e {
    E::Some(num) => dbg!(num),
    _ => todo!(),
};
```

```console
[src/main.rs:11] num = 5

```
<!-- the new line is required to avoid extra pre -->

---
<!-- .slide: data-background="hsl(20, 5%, 10%)" -->

## `λ`-funktiot (klosuurit)

<div class="ferris-container">
<img src="https://rustacean.net/assets/rustacean-flat-happy.svg" style="top: 1em; left: 10em" class="ferris">
</div>

```rust
let luvut = (1..12).filter_map(
	|n| if n % 3 == 0 { Some(n * 2) } else { None }
);

for n in luvut {
    println!("{n}");
}
```

---
<!-- .slide: data-background="hsl(20, 5%, 10%)" -->

## Lopuksi

<div class="container"><div class="col">

- Täyttäkää viikon 3 palautekysely
- [Loppukurssin projekti](../project.html)
- Lukekaa [kirjasta 6.2](https://doc.rust-lang.org/book/ch06-02-match.html)

</div><div class="col">

![](./projects.png)

</div></div>
