# Viikko 4

Tällä viikolla tutustutaan erilaisiin tietotyyppeihin ja niiden
määrittelemiseen.

Jos et päässyt tunnille, voit itsenäisesti lukea _kirjasta_ [kappaleet
5 ja 6](https://doc.rust-lang.org/stable/book/ch05-00-structs.html).

## Tavoitteet

- ymmärtää erilaisten tietotyyppien käyttötarkoitukset
  - `struct`
  - `enum`
- osata luoda omia tietotyyppejä
- tietää tyyppiteorian peruskäsitteitä:
  - tietotyyppi
  - yhdistelmätyyppi
  - tyyppikonstruktori
  - tyyppiparametri
  - algebrallinen tietotyyppi
	- tulotyyppi
	- summatyyppi
- tietää staattisen ja dynaamisen tyypityksen eron
- osata optionin käyttäminen ja ymmärtää `None`
- tietää `Result`in tarkoituksen ja funktionaalisen virheenhallinnan
- tietää piirteiden yleinen tarkoitus
- osata `#[derive]`
