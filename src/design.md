# Kurssin suunnitelma

<!-- toc -->

## TODO Syksy 2022

- Git materiaalia ja ohjeita ainakin projektityötä varten
- C-koodia ja esimerkkejä
- Yew peli projektipohja
- 12-strings pitää olla Fi eikä FI

## Vaatimukset

- Osaa ohjelmoinnin peruskonseptit/on käynyt jonkun ohjelmointikurssin
  ja osaa vähintään yhtä kieltä, esim Pythonia
- On valmis käyttämään aikaa itsenäiseen opiskeluun

## Sisältö

### Ideoita

- Materiaali sisältää Python- ja C-koodia, sekä niitä vastaavaa
  Rust-koodia
- Mahdollisimman paljon esimerkkikoodia
- Tehtävät palautetaan vasta kurssin lopussa
- Esimerkkivastaukset tulevat saataville viikottain
  - Vastausten kopiointia ei estetä, koska lukiossa ei ole niin
    tarkkaa
- Ohjelmistokehityksen ja ohjelmoinnin näkökulma kielen
  yksityiskohtien edellä
- Gitin käyttö on sallittua mutta ei pakollista

## Tehtävät

### Arviointi ja automaattinen testaus

- testit voidaan arvioinnissa lisätä tiedoston perään
  - uniikeilla funktionimillä voidaan välttyä ongelmista jos
    opiskelija on muuttanut testejä

## Lähteet

- 

<quote>

## Tuntisuunnitelma

<details>
<summary></summary>

### Ennen kurssin alkua
- Ohjeet rustin asentamiseen (`rustup`)
- Hello world itsenäisesti
- [Book 1](https://doc.rust-lang.org/book/ch01-00-getting-started)
- [Kurssialusta](rust.fablab.rip)

</details>

</quote>

### Viikko 1

Ke 13.4

#### Tunnilla
- rust asennus jos ei vielä tehty
- kurssin käytännöt
- guessing_game itsenäisesti

Keskiviikon tunnilla käydään kurssin käytännöt ja asennetaan
rust. Sitten jatketaan suoraan guessing_gamesta ja tehdään sitä
lopputunti. Kotitehtäväksi jää guessing gamen tekeminen loppuun.

Perjantain tuntia ei ole. ~~tunnilla käydään rustin perusteet diat läpi guessing_game
apuna/pohjana. Lopputunti käytetään otarustlings lataamiseen ja
rust.fablab.rip tutustumiseen.~~

#### Tehtävät
- guessing_game loppuun
- monivalintatehtävä liittyen guessing gameen: quiz 1
- viikkopalaute

#### Lähteet
- book 2

### Viikko 2

Ke 20.4, pe 22.4

#### Tunnilla
- muuttujien ja tyyppien syntaksi
  - erikoismerkit `; :: {} & ->`
- muuttujat, muuttuvuus
- funktiot
- skooppi
- omistajuus
  - lainaus pikaisesti

Keskiviikon tunnilla käydään rustin syntaksia, muuttujia ja
omistajuutta. Lopputunnilla ja perjantain tunnilla tehdään tehtäviä

#### Tehtävät
- otarustlings viikko 2
  - variables3
  - variables4
  - variables5
  - variables6
  - TODO omia
  - functions2
  - move_semantics1
  - move_semantics5
  - if1
  - if2
  - TODO omia + oma quiz
- yhteen laskettu aika noin 2h

#### Lähteet
- book 3

### Viikko 3

#### Tunnilla
- stack ja heap
- omistajuutta tarkemmin
- lainauksen ergonomia
  - esimerkki omistajuuden siirtämisestä https://doc.rust-lang.org/book/ch04-01-what-is-ownership.html#return-values-and-scope
  - miksi lainausta tarvitaan? eikä kaiken voisi tehdä siirtämällä?
- slice
- referenssit
  - box
- vektorit, stringit
- muistialuepiirrokset

#### Tehtävät
- rustlings
  - primitive types 4
  - eti hyvä string/str tehtävä
- str ja string
- jotain box
- jotain vec ja slice kombo
- jotain vec ownership/borrowing juttuja
- branched lifetime logiikka
- jotain perus käteviä makroja: dbg!, todo!, panic!
- docs.rs
- quiz joka sisältää docs.rs tehtävän
- vecdeque slice tehtävä
- tehtävä/quiz: miksi &str ei voi muuttaa takaisin &String:iksi
- enemmän tehtäviä
- tuukalle extra extra hard bonus tehtäviä

#### Lähteet
- book 4, 5, 8

### Viikko 4

#### Tunnilla
- struct, enums
  - mikä ero olion/dictionaryn kanssa
- tyyppiteoriaa
  - staattinen vs dynaaminen tyypitys
- funktionaalinen ohjelmointi yleisesti
- option
- (semanttinen tyypitys)
- invariantit
- määrittelemätön käytös
- match, if let, while let
- (iteraattorit)
- #[derive] https://doc.rust-lang.org/book/appendix-03-derivable-traits.html
- practice.rs
- ulkoiset riippuvuudet

#### Tehtävät

- klosuuri tuukalle https://github.com/matklad/rust-course/blob/master/lectures/05-functions/slides.pdf
- semanttisesta tyypityksestä r4r 76
- rust bible 3.3.1 newtype

##### practice.rs

- pattern match ref vs & https://practice.rs/ownership/borrowing.html#ref
- string indexing https://practice.rs/compound-types/string.html#string-index
- array 2, 3, 6 https://practice.rs/compound-types/array.html
- slice 2 https://practice.rs/compound-types/slice.html

##### Cargo/crate tehtävät

- serde
  - toml
- reqwest

#### Lähteet
- book 5, 6, 13
- practice.rs

### Viikko 5

Oma projekti alkaa

#### Tunnilla

Lyhyt esitys aiheista:
- semanttinen versiointi
- open source
- moduulit, (cratet)
  - private, pub, pub(crate)
- bin vs lib
- testaaminen, assert, cargo test
- virheet: Result, panic
- piirteet
  - iteraattori

Projektien esittely:
- helpompi io projekti kirjasta https://doc.rust-lang.org/book/ch12-00-an-io-project.html
- vaikeampi projekti kirjasta https://doc.rust-lang.org/book/ch20-00-final-project-a-web-server.html
- ruokalista api cli
- web yew snake game etc (meiltä pohja)
- otarustlingsiin lisää ominaisuuksia
- 

#### plain tehtävät
aiheina ainakin: result, option, ?, Error trait

- vaikeampia rustlingseja
- from_into
- from_str
- as_ref(_mut)
- advanced_errs1
- iterators1
- iterators2
- iterators3
- iterators4
- iterators5
- advanced_errs2
- semver
  - practice.rs 13.2.2 jälkeen

#### practice.rs

- 13.2.1 basic results
- 13.2.2 ParseIntError
- 10.3.4 traits
- 10.3.5 traits
- 10.3.6 traits
- 12.2.3 errors
- 13.1.2 panic
- 14.2 module crate tehtävä

#### crate tehtävät
- bin tehtävä johon pitää lisätä lib?
- binary search tree
  - jaetaan eri tehtäviksi?
	- esim usea tiedosto kirjastossa joissa jokaisessa testit ja lib.rs moduulit kommentoitu pois

#### bonus tehtävät
- macros3
- macros4
- try_from_into
- mem::take https://rust-unofficial.github.io/patterns/idioms/mem-replace.html

#### Lähteet

- book 7, 9, 10
- practice.rs
