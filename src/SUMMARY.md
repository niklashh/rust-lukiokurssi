# SUMMARY

[Johdanto](introduction.md)

---


- [Viikko 1](week1/introduction.md)
  - [Diat](week1/slides.md)
  - [Tehtävät](week1/exercises.md)
  - [Yhteenveto](week1/summary.md)

- [Viikko 2](week2/introduction.md)
  - [Diat](week2/slides.md)
  - [Tehtävät](week2/exercises.md)
  - [Yhteenveto](week2/summary.md)
  <!-- - [Esimerkkiratkaisut](week2/answers.md) -->

- [Viikko 3](week3/introduction.md)
  - [Diat](week3/slides.md)
  - [Tehtävät](week3/exercises.md)
  - [Yhteenveto](week3/summary.md)
  <!-- - [Esimerkkiratkaisut](week3/answers.md) -->

- [Viikko 4](week4/introduction.md)
  - [Diat](week4/slides.md)
  - [Tehtävät](week4/exercises.md)
  - [Yhteenveto](week4/summary.md)
  <!-- - [Esimerkkiratkaisut](week4/answers.md) -->

- [Viikko 5](week5/introduction.md)
  - [Diat](week5/slides.md)
  - [Tehtävät](week5/exercises.md)
  - [Yhteenveto](week5/summary.md)
  - [Esimerkkiratkaisut](week5/answers.md)

<!-- - [Viikko N](template_week/introduction.md) -->
<!--   - [Diat](template_week/slides.md) -->
<!--   - [Tehtävät](template_week/exercises.md) -->
<!--   - [Yhteenveto](template_week/summary.md) -->
<!--   - [Esimerkkiratkaisut](template_week/answers.md) -->

[Kalenteri](calendar.md)
[Lisämateriaali](extra.md)
[Kurssiprojekti](project.md)
[Sanasto](glossary.md)
<!-- [Verkkoympäristö](rust-code-server.md) -->
[Tehtäväalusta](otarustlings.md)
<!-- [Suunnitelma](design.md) -->
[Käsitekaavio](mindmap.md)
[Jätä palautetta](fb.md)
