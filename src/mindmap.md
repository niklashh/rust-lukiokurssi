# Kurssin käsitekaavio

- Tietokaavio kuvaa mitkä käsitteet vaativat oppiakseen toisen
  käsitteen osaamista
- Solmut vastaavat käsitteitä, jotka sisältävät tietoa sekä
  syntaksista että semantiikasta. Käsitteet eivät ole
  ali-/ylikäsitejärjestyksessä. Käsitteen vaadittu osaamistaso voi
  myös vaihdella; sen voi ehkä päätellä siitä riippuvien käsitteiden
  määrästä
- Nuolet kulkevat riippuvuuden suuntaan (lähtökäsite riippuu
  maalikäsitteestä)
- Nuolen vahvuudet on luokiteltu seuraavasti:
  - Ei nuolta: käsitteet voidaan opettaa eri järjestyksessä
  - Tavallinen: käsite on hyvä *tietää* ennen seuraavaa
    - Esimerkiksi **muuttuvuudesta** voi puhua yleisemmin, kuin vain
      **muuttujien** yhteydessä. Muuttujan käsitteen voi myös oppia
      "takaperin" jos ymmärtää muuttuvuuden
  - Vahva: käsite on hyvä *ymmärtää* ennen seuraavaa
    - Esimerkiksi **lainaamista** on turha opettaa, koska sen
      määritelmä on hyvin **omistajuus**keskeinen
- Komposiittinuolia ei tarvitse piirtää, koska osaaminen on useimmiten
  transitiivista

> "on hyvä" jää tässä sen kummemmin määrittelemättä

## Viikko 1

Viikko 1 sisältää ainoana oikeana tehtävän guessing_gamen
implementoinnin, jonka ohessa oppiii `cargo`n ja `println!` käytön.

```mermaid
flowchart LR
    linkStyle default stroke-width:1px
    guessing_game
    println[println-makro]
    cargo
    muuttujat
    muuttuvuus
    match
    laatikot
    
    guessing_game --> println & cargo & muuttujat & muuttuvuus & match & laatikot
```

## Viikko 2

Viikko 2 sisältää aloittelijatason käsitteitä Rustista. Kaikki
käsitteet liittyvät muuttujiin, joten niistä aloitetaan.

```mermaid
flowchart LR
    linkStyle default stroke-width:1px
    muuttujat
    muuttuvuus
    to[tiedon omistajuus]
    primitiivityypit
    fn[funktioiden määritteleminen]
    args[parametrit, argumentit]
    expr[lausekkeet ja puolipiste]
    if[if-lauseke]
    palauttaminen
    scope
    lainaaminen
    
    to --> muuttujat
    muuttuvuus --> muuttujat
    expr --> muuttujat
    palauttaminen --> expr
    args ==> muuttujat
    scope --> muuttujat
    fn ==> primitiivityypit & args
    fn --> expr & scope & expr
    if --> expr
    lainaaminen ==> to
```

## Viikko 3

Viikko 3 sisältää tutustumisen muistinhallintaan ja tavuihin.

```mermaid
flowchart LR
    linkStyle default stroke-width:1px
    kutsupino
    koko
    representaatio
    slice
    keko
	taulukko
    vec[Vec-tietorakenne]
    referenssi
    osoitin
    proc[prosessit ja säikeet]
    ohjelmatiedosto
    konekieli
    
    vec --> osoitin
    keko --> koko & kutsupino
    osoitin --> keko
    taulukko --> osoitin
    slice --> taulukko & representaatio --> koko
    referenssi --> osoitin
    referenssi ==> lainaaminen ==> to[tiedon omistajuus]
    proc --> ohjelmatiedosto --> konekieli
```

## Viikko 4

Viikko 4 tutustutaan funktionaalisesta ohjelmoinnista tuttuihin
algebrallisiin tietotyyppeihin (struct ja enum) ja niiden
esitykseen muistissa.

```mermaid
flowchart LR
    linkStyle default stroke-width:1px
	tuplet
	structit
	enumit
	Option
	Result
	at[algebrallinen tietotyyppi]
	staticdyn[staattinen ja dynaaminen tyypitys]
	virheenhallinta
	panikointi
	hashmap[HashMap-tietorakenne]
	match
	derive
    klosuurit
    assocfn[metodit ja assosioidut funktiot]
    impl
    traits[piirteet yleisellä tasolla]
    
    tuplet & structit --> at
    Option & Result --> enumit ==> structit & tuplet
    assocfn --> at
    virheenhallinta --> Option & Result & panikointi
    hashmap ==> Result & Option
    match ==> at
    impl --> enumit
    impl ==> assocfn
    derive --> traits
    klosuurit ==> fn[funktioiden määritteleminen]
```

> Enumit liittyvät structeihin, koska struct-syntaksia käytetään
> enumien struct-varianteissa

## Solmut

- moduulit
- laatikot
- `Cargo.toml`
- virhepropagointi `?`
- kutsupinon purku (unwind)
- geneeriset tyypit, tyyppifunktiot
- piirteet yleisellä tasolla
- geneeristen rajoittaminen piirteillä
- `dyn Trait`
- `pub`
- `Box` smart pointer
- bin/lib

