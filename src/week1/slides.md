
<!-- The first line must be blank... -->
<!-- .slide: data-background="#182B23" -->

## Rust-kurssi

```rust
fn start_course() {
    println!("Course starting countdown:");
    for i in (1..=3).rev() {
      println!("{}", i);
    }
}

start_course();
```

---
<!-- .slide: data-background="#23182B" -->

## ![](rustacean-flat-happy.png) <!-- .element: width="30%" style="position:absolute; top:-1.5em; left:0;" --> Kysymyksiä

- Mitä ohjelmointikieliä osaat? <!-- .element: class="fragment" -->
- Miksi haluat oppia Rustia? <!-- .element: class="fragment" -->
- Oletko käyttänyt Rustia ennen? <!-- .element: class="fragment" -->

---
<!-- .slide: data-background="#2B2318" -->

## Käytännöt

- Tunnit `ma` ja `ke` `15-16`
  - `ma` pidetään luennot
  - `ke` tehdään tehtäviä
- Tehtäviä tehdään sekä tunnilla että itsenäisesti <!-- .element: class="fragment" -->
- Ei kokeita, ei arvosanaa <!-- .element: class="fragment" -->
- Kurssiprojekti <!-- .element: class="fragment" -->

---
<!-- .slide: data-background="#620F4F" -->

# ~Rust kerho~

- Peruttu

<style> marquee { pointer-events: none; } </style>

<marquee behavior="alternate" scrollamount="1" scrolldelay="11" truespeed="true" direction="down" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="13" truespeed="true" direction="left" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="50px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="12" truespeed="true" direction="down" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="14" truespeed="true" direction="right" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="50px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="10" truespeed="true" direction="up" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="12" truespeed="true" direction="left" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="50px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="9" truespeed="true" direction="up" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="11" truespeed="true" direction="right" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="38px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="15" truespeed="true" direction="down" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="20" truespeed="true" direction="left" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="50px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="9" truespeed="true" direction="down" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="12" truespeed="true" direction="right" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="55px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="15" truespeed="true" direction="up" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="15" truespeed="true" direction="left" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="45px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="8" truespeed="true" direction="up" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="23" truespeed="true" direction="right" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="40px">
</marquee>
</marquee>

<marquee behavior="alternate" scrollamount="1" scrolldelay="1" truespeed="true" direction="up" style="height: 100vh; position: absolute; left: 0; top: 0">
<marquee behavior="alternate" scrollamount="1" scrolldelay="3" truespeed="true" direction="right" style="width: 100vw">
<img src="https://rustacean.net/assets/rustacean-flat-noshadow.png" height="30px">
</marquee>
</marquee>


---
<!-- .slide: data-background="#221F2F" -->

## Kurssimateriaali

[otafablab.gitlab.io/rust-lukiokurssi](https://otafablab.gitlab.io/rust-lukiokurssi)

![](rustacean-flat-gesture.png) <!-- .element: width="30%"-->

---
<!-- .slide: data-background="#182B23" -->

## Rustin asennus

- [doc.rust-lang.org/book](https://doc.rust-lang.org/book/) kappale 1
  - [rustup.rs](https://rustup.rs)
- Linux käyttäjille: asenna `rustup` paketinhallinasta
- Lisätehtävä
  - [TourOfRust](https://tourofrust.com/chapter_1_en.html) kappale 1
