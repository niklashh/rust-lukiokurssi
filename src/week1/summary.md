# Yhteenveto

Ensimmäisellä viikolla tavoitteena oli tutustua Rustiin guessing gamen avulla.

## Viikkopalaute

Joka viikolla voi ja kannattaa täyttää palautelomake, jonka avulla
opettajat voi päätellä mitä pitäisi opettaa enemmän

[Anna palautetta miten sinulla meni tämän viikon aiheet](https://docs.google.com/forms/d/e/1FAIpQLSeNcTmFBchSnGPV691A2GKs-6CSTp7Toi9dFctGznY1stm6-g/viewform?usp=sf_link)
