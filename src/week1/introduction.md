> Tämä dokumentti on tarkoitettu ohjeeksi opiskelijoille, jotka eivät ehkä päässeet tunnille.
> Asiat käydään tunnilla läpi

# Viikko 1

## Kehitysympäristö

Kurssilla on käytössä VSCode-pohjainen verkkokehitysalusta. [Lue lisää](/rust-code-server.md)

## Rustin asennus

Rustin asentaminen tapahtuu `rustup`-ohjelmalla, joka asentaa Rust-kääntäjän lisäksi cargon. Asenna [`rustup.rs`](https://rustup.rs/).

Halutessasi voit lukea _kirjan_ kappaleen [Rustin asentamisesta](https://doc.rust-lang.org/book/ch01-01-installation.html).

## Rustin _kirja_

[_The Rust Programming Language_](https://doc.rust-lang.org/stable/book/), tunnetaan myös nimellä _The Book_, on Steve Klabnikin ja Carol Nicholsin kirjoittama Rustin virallinen opetusmateriaali.

_Kirja_ on Rust-koodarille, kuin `MAOL` datalle. _Kirja_ sisältää oppia kaikista Rustin tärkeimmistä ominaisuuksista ja paljon yleistä tietoa ohjelmoinnista yleensä. _Kirja_ on tämän kurssin tärkein lähde, ja sitä luetaan joka viikolla.
