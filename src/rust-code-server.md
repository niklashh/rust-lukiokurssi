# [`rust-code-server`](https://example.com)

![kuva](https://gitlab.com/otafablab/rust-code-server/-/raw/main/screenshot.png)

## **Huomio:**

Rust-code-server ei ole tällä hetkellä käytössä, vaan on päällä
ainoastaan kurssin aikana.

Jos olet tallentanut tietoa rust-code-serveriin, johon tarvitset vielä
pääsyn, ota yhteyttä kurssin järjestäjiin: `niklas.2.halonen (at)
aalto.fi`, `matias.zwinger (at) aalto.fi`.

Online visual studio code verkkosivu, jossa tarvittavat Rust
lisäösat + tyylikäs teema.

Rust-code-serveriin kirjaudutaan `eduespoo`-tunnuksilla.

## Varoitus

Älä tallenna rust code serveriin mitään yksityistä, salaista (kuten
ssh avaimia) tai muuten tärkeää tietoa. Opettajilla on pääsy kaikkeen
tietoon mitä tallennat rust code serverille.

### Vastuuvapaus

Rust code serveriä tulee käyttää omalla vastuulla. Emme takaa
tiedostojen säilymistä tai palvelun tietoturvallisuutta tai
toimivuutta.

### Puutteet

Tällä hetkellä havaitut puutteet:
- Gitin käyttö

## Lähdekoodi

- Podman kontti: <https://gitlab.com/otafablab/rust-code-server>
- Rust-pohjainen websocket reverse proxy container orchestrator:
  <https://gitlab.com/otafablab/rcs-backend>
