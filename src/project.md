# Kurssiprojekti

Kurssiprojektin tarkoitus on mahdollistaa luovuuden maksimaalinen
käyttö, jotta Rustin (ja ohjelmoinnin) oppiminen olisi mahdollisimman
luontevaa.

Projektin laajuus riippuu täysin opiskelijan taitotasosta ja käytössä
olevasta ajasta.

## Oma idea (suositeltu)

Kaikista paras projekti on sellainen, jolle opiskelijalla on tarve,
kuten pieni ohjelma tai skripti/automaatio.

Paras projekti palkitaan!

Jos sellaista ei ole mielessä, voit lukea seuraavia ideoita:

## Valmiit projekti-ideat

### Peli

Rustissa on muutama suosittu pelikehys:

- [bevy](https://bevyengine.org/)
- [piston](https://www.piston.rs/)
- [amethyst](https://amethyst.rs/)
- [SDL2](https://github.com/Rust-SDL2/rust-sdl2)

[Lisää tietoa](https://arewegameyet.rs/)

Komentorivipohjaista peliä varten on [`console_engine`](https://github.com/VincentFoulon80/console_engine)

### Lisp-projekti

Tee oma Lisp-tulkki. Tutoriaali:
<https://vishpat.github.io/lisp-rs/overview.html>

Esimerkki Lisp-koodista:

```lisp
(
	(define m 10)
	(define n 12)
	(define K 100)
	
	(define func1 (lambda (x) (+ x K)))
	(define func2 (lambda (x) (- x K)))
	
	(func1 m)
	(func2 n)
)
```

### Koulun ruokalista työkalu

Ohjelma, joka näyttää viikon ruokalistan [REST
API](https://www.amica.fi/api/restaurant/menu/day?date=2022-5-13&language=fi&restaurantPageId=330303)a
käyttäen

### Mini-redis

Käyttäen `tokio`-asynkronista runtimeä

<https://tokio.rs/tokio/tutorial>

### IO-projekti kirjasta

Pieni `grep` implementaatio (komentorivityökalu)

<https://doc.rust-lang.org/stable/book/ch12-00-an-io-project.html>

### Loppuprojekti kirjasta (vaikea)

Monisäikeinen verkkopalvelin

<https://doc.rust-lang.org/stable/book/ch20-00-final-project-a-web-server.html>

## Projektipohjat

### Matopeli yewillä

TODO

## Muut projekteiksi käyvät vaihtoehdot

- [Fablabin GitLab projektien kehitys](https://gitlab.com/otafablab)
  - [rip-portal](https://gitlab.com/otafablab/rip-portal) on
    3D-tulostimien verkkoportaali
  - [otarustlings](https://gitlab.com/otafablab/otarustlings) on
    Rust-kurssin tehtäväalusta
- Tutustu johonkin Rustin ekosysteemin merkittävimpiin projekteihin:
  - [`serde`](https://serde.rs/)
  - [`rocket`](https://rocket.rs/)
  - [`tracing`](https://docs.rs/tracing/latest)
  - [`yew`](https://yew.rs/)
  - [`ripgrep`](https://github.com/BurntSushi/ripgrep)
  - [`itertools`](https://docs.rs/itertools/latest)
